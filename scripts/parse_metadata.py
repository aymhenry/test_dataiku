"""
This script extracts the following metadata for the dataset features:
- Number of unique values
- variable name
- variable type (nominaml or continuous)
"""

import pandas as pd
import re
from loguru import logger
import argparse
import yaml

uniquelval_type_regex = re.compile("(\d+) distinct values for attribute #\d+ \((.*?)\) (\w+)")

def extract_metadata(input_path, output_path):
    """
    Extracts the metadata from the input file and saves it to the output file
    """
    ## Read the metadata file
    with open(input_path, "r") as file:
        raw_line_list = file.readlines()
    logger.info(f"Read {len(raw_line_list)} lines from {input_path}")

    ## search_variable_metadata
    def search_uniquelval_type(line):
        """
        Search for the metadata containing the number of unique values, variable name and variable type
        """
        match = uniquelval_type_regex.search(line)
        if match:
            return {"unique_values": int(match.group(1)), "variable_name": match.group(2), "variable_type": match.group(3)}
        else:
            return None

    ## Check every line in the metadata file for metadata
    metadata = [search_uniquelval_type(line) for line in raw_line_list]

    ## Keep only the non-None values and store them in a dataframe
    metadata = pd.DataFrame([m for m in metadata if m is not None])
    logger.success(f"Found number of unique and types for {metadata.shape[0]} variables")

    ## Add Goal variable
    metadata = pd.concat([metadata, pd.DataFrame([{"unique_values": 2, "variable_name": "total person income bin", "variable_type": "nominal"}])], ignore_index=True)
    logger.info(f"Added \"total person income bin\" to the metadata")
    ## Save the metadata to a csv file
    metadata.to_csv(output_path, index=False)
    logger.info(f"Saved metadata to {output_path}")

if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("--param", type=str, default="params.yaml", help="Path to the study parameters")
    args = argument_parser.parse_args()

    with open(args.param, "r") as file:
        params = yaml.safe_load(file)
        input_path = params["metadata_path"]
        output_path = params["column_metadata_path"]

    extract_metadata(input_path, output_path)
