"""
This scripts downloads the data from the project
"""

DATA_URL = "http://thomasdata.s3.amazonaws.com/ds/us_census_full.zip"
DATA_DIR = "data"

import os
import zipfile
import requests
from loguru import logger

def download_data(url=DATA_URL, dir=DATA_DIR):

    if not os.path.exists(dir):
        os.makedirs(dir)
    data = requests.get(url)
    zip_file = url.split("/")[-1]   
    with open(os.path.join(dir, zip_file), "wb") as f:
        f.write(data.content)
    with zipfile.ZipFile(os.path.join(dir, zip_file), "r") as zip_ref:
        for file in zip_ref.filelist:
            if file.filename[-4:] in [".csv",".txt"]:
                base_ilename = os.path.basename(file.filename)
                zip_ref.extract(file, os.path.join(dir, base_ilename))
                logger.info(f"Extracted {file.filename} to {os.path.join(dir, base_ilename)}")


    os.remove(os.path.join(dir, zip_file))

if __name__ == "__main__":
    download_data(DATA_URL, DATA_DIR)