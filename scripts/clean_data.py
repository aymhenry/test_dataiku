"""
This script is used to clean the data.

The data is cleaned by:
1. Dropping the instance weight column
2. Renaming the columns to the variable names in the metadata (easier to use)
3. Removing duplicate rows
4. Removing contradictory rows

"""
import pandas as pd
from loguru import logger
import yaml
import argparse
## The Column of the instance weight is hard coded here
INSTANCE_WEIGHT_COLUMN = 24

def clean_dataset(data_path, output_path, metadata_path, check_consistency=True):
    data = pd.read_csv(data_path, header=None)
    logger.info(f'Read the data ({data_path}) with shape {data.shape}')
    metadata = pd.read_csv(metadata_path)
    logger.info(f'Read the metadata')

    # Drop the instance weight column
    data = data.drop(columns=[INSTANCE_WEIGHT_COLUMN])

    logger.success(f'Dropped the instance weight (column { INSTANCE_WEIGHT_COLUMN })')

    # Rename the columns
    rename_dict = {}
    for i,col_ind in enumerate(data.columns):
        col_meta = metadata.iloc[i]
        # Double check that we are using the correct metadata for the column
        if col_meta['unique_values'] != data[col_ind].nunique() and check_consistency:
            error_msg = f'Column {col_ind} has {data[col_ind].nunique()} unique values, but the metadata says it has {col_meta["unique_values"]}'
            logger.error(error_msg)
            raise ValueError(error_msg)
        rename_dict[col_ind] = col_meta['variable_name']

    data = data.rename(columns=rename_dict)
    logger.success('Renamed the columns')

    ## Remove duplicate rows
    old_len = len(data)
    data = data.drop_duplicates()
    logger.success(f'Removed {old_len - len(data)} duplicate rows')

    ## Remove contradictory rows
    ## Contradictory rows are rows that have the same feature values but different labels
    contradictory_rows = data.iloc[:,:-1].duplicated()
    data = data.loc[~contradictory_rows]
    logger.success(f'Removed {contradictory_rows.sum()} contradictory rows')
    logger.info(f'Removed a total of {old_len - len(data)} rows')
    data.to_csv(output_path, index=False)
    logger.info(f'Saved the cleaned data to {output_path}')
    return data

if __name__ == '__main__':
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("--param", type=str, default="params.yaml", help="Path to the study parameters")
    args = argument_parser.parse_args()

    with open(args.param, "r") as file:
        params = yaml.safe_load(file)
        column_metadata_path = params["column_metadata_path"]
        train_data_path = params["train_data_path"]
        cleaned_train_data_path = params["cleaned_train_data_path"]
        test_data_path = params["test_data_path"]
        cleaned_test_data_path = params["cleaned_test_data_path"]

    clean_dataset(train_data_path, cleaned_train_data_path, column_metadata_path)
    logger.success(f'Cleaned the training data')
    clean_dataset(test_data_path, cleaned_test_data_path, column_metadata_path,False)
    logger.success(f'Cleaned the test data')