
This a binary classification problem. The goal is to predict whether a person makes over 50K a year.

To find a a presentation of the project, please refer to the file [presentation.pdf](presentation.pdf).   

## install the requirements

It is best to create a virtual environment for this project. 

To install the requirements, run the following command:
```bash
pip install -r requirements.txt
```

## Download the data

The data are available at the following URL: `http://thomasdata.s3.amazonaws.com/ds/us_census_full.zip`.

A script is available to download the data and unzip it. Run the following command to download and unzip the data:
```bash
python scripts/download_project_data.py
```


## Parse the metadata

The data files do not contain any metadata. The metadata is stored in the file `data/census_income_metadata.txt`.
There are two metadata that are of particular interest:
- the column names
- number of unique values for each column (for debugging purposes)

The following code will parse the metadata to make them available for use:

```python
python scripts/parse_metadata.py
```
The output will be a file `data/column_metadata.csv`.

## Clean the data

The raw data do not have its columns labeled and it contains several elements that need to be removed. the steps to clean the data are as follows:

- Load the data
- Remove the rows containing instance weights (column #24) as it should not be used for income prediction
- Remove the duplicate rows (keep the first occurrence)
- Remove the contradictory rows (that have the same feature values but different income values)
- Save the new data to a new file

The following scipts will clean the data:

```bash
python scripts/clean_data.py
```

## The study

The study is divided into two parts, each contained in a separate notebook:

- The first part is an exploratory data analysis (EDA) to understand the data and the relationships between the features. The notebook is [Exploratory_analysis.ipynb](Exploratory_analysis.ipynb).

- The second part is the model building and evaluation. The notebook is [Data_modeling.ipynb](Data_modeling.ipynb).